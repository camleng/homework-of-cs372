<?php
  function hex2int($str) {
    $digits = "0123456789ABCDEF";
    $val = 0;
    $str = strtoupper($str);
    for ($i=0; $i<strlen($str); $i++) {
      $char = $str{$i};
      $digit = strpos($digits, $char);
      $val = 16*$val + $digit;
    }
    return $val;
  }

  echo hex2int("1e") . "<br>";
  echo hex2int("a") . "<br>";
  echo hex2int("11ff") . "<br>";
  echo hex2int("cceeff") . "<br>";
?>
