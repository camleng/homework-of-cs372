-- phpMyAdmin SQL Dump
-- version 4.5.2
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Nov 17, 2016 at 03:59 PM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `cs372`
--

-- --------------------------------------------------------

--
-- Table structure for table `Review`
--

CREATE TABLE `Review` (
  `id` int(11) NOT NULL,
  `name` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `referrer` varchar(10) NOT NULL,
  `rating` varchar(5) NOT NULL,
  `comments` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `Review`
--

INSERT INTO `Review` (`id`, `name`, `email`, `referrer`, `rating`, `comments`) VALUES
(1, 'Tom', 'tom@ipfw.edu', 'Other', 'maybe', 'Another good review!'),
(2, 'Zesheng Chen', 'chenz@ipfw.edu', 'Google', 'yes', 'good day!'),
(3, 'Zach', 'zach@ipfw.edu', 'Advert', 'no', 'No good enough'),
(4, 'Daniel', 'daniel@ipfw.edu', 'Other', 'maybe', 'Maybe good, maybe bad!'),
(5, 'Josephine Chen', 'josephine@ipfw.edu', 'Friend', 'yes', 'I like it so much!'),
(6, 'Cameron Lengerich', 'cameron.lengerich@gmail.com', 'Other', 'yes', 'This is a totally great site!'),
(7, 'Brett Affolder', 'affolderbrett@gmail.com', 'Friend', 'yes', 'I love it!');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Review`
--
ALTER TABLE `Review`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Review`
--
ALTER TABLE `Review`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
