<style>
  table, th, td {
    border: 1px solid black;
  }
</style>

<?php

require_once('connect_to_db.php');

$sql = 'SELECT * FROM Review;';
$result = $conn->query($sql);
echo "<table><thead><tr><th>id</th><th>name</th><th>email</th><th>referrer</th><th>rating</th><th>comments</th></thead><tbody>";
if ($result->num_rows > 0) {
  while ($row = $result->fetch_assoc()) {
    echo "<tr><td>" . $row["id"] . "</td><td>" . $row["name"] . "</td><td>" . $row["email"] . "</td><td>" . $row["referrer"] . "</td><td>" . $row["rating"] . "</td><td>" . $row["comments"] . "</td></tr>";
  }
}

?>
