<style>
  table, th, td {
    border: 1px solid black;
  }
</style>

<?php
  require_once('connect_to_db.php');

  $sql = "CREATE TABLE IF NOT EXISTS Schedule(
    Semester VARCHAR(20) NOT NULL,
    Course VARCHAR(50) NOT NULL);";

  if ($conn->query($sql) === FALSE) {
    echo "Error creating table: " . $conn->error . "\n";
  }

  $sql = "INSERT INTO Schedule VALUES
  ('Fall 2016', 'Web Application Development'),
  ('Spring 2016', 'Computer Networks');";

  if ($conn->query($sql) === FALSE) {
    echo "Error inserting records: " . $conn->error . "\n";
  }

  $sql = "SELECT * FROM Schedule;";
  $result = $conn->query($sql);
  echo "<table><thead><tr><th>Semester</th><th>Course</th></thead><tbody>";
  if ($result->num_rows > 0) {
    while ($row = $result->fetch_assoc()) {
      echo "<tr><td>" . $row["Semester"] . "</td><td>" . $row["Course"] . "</td></tr>";
    }
  }
  echo "</tbody></table>";
  $conn->close();
?>
