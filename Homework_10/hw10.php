<?php
    if (isset($_POST["submit"])) {
      if (empty($_POST["name"]) ||
        empty($_POST["email"]) ||
        empty($_POST["referral"]) ||
        empty($_POST["visitAgain"]) ||
        empty($_POST["comments"]))
      {
        $error = true;
      }
      else {
        require_once('connect_to_db.php');

        $sql = "INSERT INTO Review (name, email, referrer, rating, comments) VALUES
        ('" . $_POST["name"] . "', '" . $_POST["email"] . "', '" . $_POST["referral"] . "', '" . $_POST["visitAgain"] . "', '" . $_POST["comments"] . "');";

        if ($conn->query($sql) === FALSE) {
          echo "Error inserting records: " . $conn->error . "\n";
        }

        $conn->close();

        header('Location: show_review.php');
      }
    }
?>


<!DOCTYPE html>
<html>
<head>
    <title>Forms</title>
</head>
<body>
    <?php if (isset($error))
    		echo "<div style='color: red'>You must fill out the form!</div>";
    ?>
    <form action="hw10.php" method="post">
        <fieldset>
            <legend>Your Details:</legend>
            Name: <input type="text" name="name"
            value = "<?php if (isset($_POST["name"]))
  						echo htmlspecialchars($_POST["name"]); ?>"><br>
            Email: <input type="text" name="email"
            value = "<?php if (isset($_POST["email"]))
  						echo htmlspecialchars($_POST["email"]); ?>">
        </fieldset>

        <br>

        <fieldset>
            <legend>Your Review:</legend>
            <p>How did you hear about us?
            <select name="referral">
              <!-- google, friend, advert, other -->
              <?php
                $referrals = array("Google", "Friend", "Advert", "Other");
                foreach ($referrals as $ref) {
                  if (isset($_POST["referral"]) && $_POST["referral"] == $ref)
                    echo "<option selected='selected' value='$ref'>" . $ref . "</option>";
                  else
                    echo "<option value='$ref'>" . $ref . "</option>";
                }
              ?>
            </select>
            </p>

            Would you visit again?<br>
            <input type="radio" name="visitAgain" value="yes"
            <?php if ((isset($_POST["visitAgain"]) && $_POST["visitAgain"] == "yes"))
                  echo "checked"; ?>
            >Yes
            <input type="radio" name="visitAgain" value="no"

            <?php if ((isset($_POST["visitAgain"]) && $_POST["visitAgain"] == "no"))
                  echo "checked"; ?>
            >No
            <input type="radio" name="visitAgain" value="maybe"
            <?php if ((isset($_POST["visitAgain"]) && $_POST["visitAgain"] == "maybe"))
                  echo "checked"; ?>
            >Maybe

            <br><br>

            Comments:<br>
            <textarea cols="40" rows="4" name="comments">
              <?php if (isset($_POST["comments"]))
                echo $_POST["comments"];
              ?>
            </textarea>

            <br><br>

            <input type="checkbox" name="signup">Sign me up for email updates<br>
            <input type="submit" name="submit" value="Submit review">

        </fieldset>
    </form>
</body>
</html>
