### 1.	Dig or nslookup

**A.	Find the IP addresses of the Web server of our university.**

- `52.205.6.225`



**B.	Find the names and IP addresses of the domain name servers of our university.**

- dns1.iu.edu		`134.68.220.8`
- dns1.ipfw.edu	`149.164.1.1`
- dns2.ipfw.edu	`149.164.255.253`

**C.	Find the names and IP addresses of the mail server of our university.**

- mailsec1.ipfw.edu	`149.164.5.41`

- mailsec2.ipfw.edu	`149.164.233.4`

**D.	Determine the IP address range of our university.**

- `149.164.0.0` - `149.164.255.255`




###  2.	Telnet and HTTP

```sh
echo "open $1 80"
sleep 2
echo "GET $2 HTTP/1.1"
echo "Host: $1"
echo
echo
sleep 2
```



### 3.	Web Service

**A.	Fort Wayne, IN**

- Latitude: `41.079273`


- Longitude: `-85.1393513`

**B.	Miami, FL**

- Latitude: `25.7616798`

- Longitude: `-80.1917902`

**C.	San Francisco, CA**

- Latitude: `37.7749295`
- Longitude: `-122.4194155`