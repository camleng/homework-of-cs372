function validate() {
  var n1 = num1.value;
  var n2 = num2.value;
  if (n1.length == 0 || isNaN(n1)) {
    alert('Number 1 is required and should be an integer');
    return false;
  } else if (n2.length == 0 || isNaN(n2)) {
    alert('Number 2 is required and should be an integer');
    return false;
  }

  return true;
}

function add() {
  if (validate()) {
    /* We need to parse them to Number for addition,
    because otherwise it will concatenate the strings.*/
    result.value = Number(num1.value) + Number(num2.value);
  }
}

function subtract() {
  if (validate()) {
    result.value = num1.value - num2.value;
  }
}

function multiply() {
  if (validate()) {
    result.value = num1.value * num2.value;
  }
}

function divide() {
  if (validate()) {
    result.value = num1.value / num2.value;
  }
}


function isChecked() {
  if (signupForEmails.checked)
    submit.classList.remove('disabled');
  else
    submit.classList.add('disabled');
}

function isEdu() {
  if (!/^.+@.+\.edu$/.test(email.value)) {
    alert("Email must be a .edu address!");
    return false;
  } else {
    alert("Thank you for signing up! We promise not to spam you.");
    return true;
  }
}
